<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$factory->define(\App\Employee::class, function (Faker $faker) {
    return [
        'is_active'=> rand(0,1),
        'age'=>rand(1,80),
        'name'=> $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'company' => $faker->word,
        'email' =>  $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
    ];
});
