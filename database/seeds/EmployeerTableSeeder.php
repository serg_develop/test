<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class EmployeerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Employee::class, 50)->create();
    }
}
