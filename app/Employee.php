<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employeer';
    protected $fillable = [
        'is_active',
        'age',
        'name',
        'gender',
        'company',
        'email',
        'phone',
        'address'
    ];
}
